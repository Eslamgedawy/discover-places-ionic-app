import { Component, OnInit, OnDestroy } from '@angular/core';
import { PlacesService } from '../places.service';
import { Place } from '../place.model';
import { MenuController } from '@ionic/angular';
import { SegmentChangeEventDetail } from '@ionic/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss'],
})
export class DiscoverPage implements OnInit , OnDestroy {

  placesSub: Subscription;
  loadedPlaces: Place[];
  listedPlaces: Place[];
  relevantPlaces: Place[];

  constructor(
    private placesServices: PlacesService,
    private authService: AuthService
    // private menuCtrl: MenuController,
  ) { }

  ngOnInit() {
    // this.loadedPlaces = this.placesServices.places;
    this.placesSub = this.placesServices.places.subscribe(places =>{
      this.loadedPlaces = places;
      this.relevantPlaces = this.loadedPlaces;
      this.listedPlaces = this.relevantPlaces.slice(1);
    })
  }

  onOpenMenu() {
    // this.menuCtrl.toggle();
  }

  // segmentChanged(ev: any) {
  //   console.log('Segment changed', ev);
  // }

  onFilterUpdate(event: CustomEvent<SegmentChangeEventDetail>) {
    if(event.detail.value === 'all'){
      this.relevantPlaces = this.loadedPlaces;
      this.listedPlaces = this.relevantPlaces.slice(1);
    }else{
      this.relevantPlaces = this.loadedPlaces.filter(place => place.userId !== this.authService.userId)
      this.listedPlaces = this.relevantPlaces.slice(1);
    }
    // console.log(event.detail);
  }

  ngOnDestroy(){
    if(this.placesSub){
      this.placesSub.unsubscribe();
    }
  }
}
