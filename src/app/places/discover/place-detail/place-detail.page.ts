import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, ModalController, ActionSheetController } from '@ionic/angular';
import { CreateBookingsComponent } from '../../../bookings/create-bookings/create-bookings.component';
import { Place } from '../../place.model';
import { PlacesService } from '../../places.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.page.html',
  styleUrls: ['./place-detail.page.scss'],
})
export class PlaceDetailPage implements OnInit , OnDestroy {

  placeSub: Subscription;
  place: Place;
  snapShot = "init";

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private placesService: PlacesService,
    private modalCtrl: ModalController,
    private actionSheetController: ActionSheetController
  ) {}

  ngOnInit() {
      // using subscription
    // this.route.paramMap.subscribe(paramMap => {
    //   if (!paramMap.has('placeId')) {
    //     this.navCtrl.navigateBack('/places/tabs/discover');
    //     return;
    //   }
    // this.placeSub = this.placesService.getPlace(paramMap.get('placeId')).subscribe(place =>{
    //   this.place = place;
    // })
    // });

      // using snapshot
    this.snapShot = this.route.snapshot.paramMap.get('placeId');
     this.placeSub = this.placesService.getPlace(this.snapShot).subscribe(place =>{
        this.place = place;
      })

    //=======
    // this.place = this.placesService.getPlace(paramMap.get('placeId'));
  }

  onBookPlace() {
    // this.router.navigateByUrl('/places/tabs/discover');
    // this.navCtrl.navigateBack('/places/tabs/discover');
    // this.navCtrl.pop();
    
      this.actionSheetController.create({
        header: 'choose an action',
        buttons: [{
          text: 'Select Date',
          // role: 'destructive',
          icon: 'arrow-dropright-circle',
          handler: () => {
           this.onBookingModal('select');
          }
        },
        {
          text: 'Random Date',
          // role: 'destructive',
          icon: 'arrow-dropright-circle',
          handler: () => {
           this.onBookingModal('random');
          }
        }
        ,{
          text: 'Cancel',
          icon: 'close',
          role: 'destructive',
          handler: () =>{
            console.log("cancel Clicked");
          }
        }
       ]
      }).then((ele)=>{
        ele.present();
      })

  }

  onBookingModal(mode: 'select' | 'random'){
    console.log(mode);
    this.modalCtrl.create({
            component: CreateBookingsComponent,
            componentProps: { selectedPlace: this.place , selectedMode: mode }            
          })
          .then(modalEl => {
            modalEl.present();
            return modalEl.onDidDismiss(); // this is for overlay event detail
          }) 
          .then(ele => {
            console.log(ele.role,ele.data); 
            if(ele.role === 'confirm'){
              console.log('Booked');
            }
          }) 
}

ngOnDestroy(){
  if(this.placeSub){
    this.placeSub.unsubscribe();
  }
}
}
