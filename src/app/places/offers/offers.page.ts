import { Component, OnInit, OnDestroy } from '@angular/core';
import { PlacesService } from '../places.service';
import { Place } from '../place.model';
import { IonItemSliding } from '@ionic/angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit , OnDestroy {

  offers: Place[];
  placsesSub: Subscription;

  constructor(
    private placesService: PlacesService , 
    private rourter: Router
  ) { }

  ngOnInit() {
    // console.log("ngOnInit");
  //  this.offers =  this.placesService.places;
   this.placsesSub = this.placesService.places.subscribe(places =>{
      this.offers = places;
    });
  }
/*
  ionViewDidEnter(){
    console.log("ionViewDidEnter");
  }
  ionViewWillEnter(){
    console.log("ionViewWillEnter");
  }
  ionViewDidLeave(){
    console.log("ionViewDidLeave");
  }
  ionViewWillLeave(){
    console.log("ionViewWillLeave");
  }
*/
  onEdit(offerId: string , ionItemSilding : IonItemSliding){
    ionItemSilding.close();
    this.rourter.navigate(['/','places','tabs','offers','edit',offerId])
    console.log("on Editing", offerId);
  } 

  ngOnDestroy(){
    if(this.placsesSub){
      this.placsesSub.unsubscribe();
    }
  }
}
