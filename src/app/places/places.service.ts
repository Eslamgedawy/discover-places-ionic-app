import { Injectable } from '@angular/core';
import { Place } from './place.model';
import { AuthService } from '../auth/auth.service';
import { BehaviorSubject } from 'rxjs';
import { take, map, tap, delay } from 'rxjs/operators';
// import { setTimeout } from 'timers';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  constructor(private authService: AuthService){

  }
  // private _places: Place[] = [];
  // new BehaviorSubject<Place[]>()
 private _places = new BehaviorSubject<Place[]>( [
    new Place(
      'p1',
      'Naser city',
      'is a district of Cairo, Egypt. It is located to the east of the Cairo Governorate and consists mostly of condominia. It was established in the 1960s as an extension to neighboring settlement of Heliopolis',
      'https://upload.wikimedia.org/wikipedia/commons/6/6a/Nasrcity.jpg',
      299.9,
      new Date('2020-01-01'),
      new Date('2020-12-31'),
      'h2z'
    ),
    new Place(
      'p2',
      'Balteem',
      'is typical to the northern coastal line which is the most moderate in Egypt. It features a hot desert climate (Köppen: BWh), but prevailing winds from the Mediterranean Sea greatly moderate the temperatures, making its summers moderately hot and humid while its winters mild and moderately wet',
      'https://upload.wikimedia.org/wikipedia/ar/thumb/4/4d/MasyafBaltim1.jpg/280px-MasyafBaltim1.jpg',
      199.9,
      new Date('2020-01-01'),
      new Date('2020-12-31'),
      'xyz'
    ),
    new Place(
      'p3',
      'Port-said',
      'is a city that lies in north east Egypt extending about 30 kilometres (19 mi) along the coast of the Mediterranean Sea, north of the Suez Canal',
      'https://upload.wikimedia.org/wikipedia/commons/9/94/Port_said_egypt_%286%29.JPG',
      99.9,
      new Date('2020-01-01'),
      new Date('2020-12-31'),
      'h2z'
    ),
  ]);
  // constructor() { }

  get places(){
    // return [...this._places];
    // return this._places;
    return this._places.asObservable();
    
  }

  getPlace(id: string){
  //  return {...this._places.find(p => p.id === id)};
  //  return this._places.find(p => p.id === id);

  // return this.places.pipe(take(1) , map(places =>{
  //   return {...places.find(p => p.id === id)};
  // }))

  return this.places.pipe(
    take(1),
    map(places =>{
     return {...places.find(place => place.id === id)};
    })
  )

  }

  addNewPlace(
    // id: string,
    title: string,
    description: string,
    // imgUrl: string,
    price: number,
    dateFrom: Date,
    dateTo: Date,
    // userId: string
  ){
    const newPlace = new Place(
      Math.random().toString(),
      title,
      description,
      'https://upload.wikimedia.org/wikipedia/commons/9/94/Port_said_egypt_%286%29.JPG',
      price,
      dateFrom ,
      dateTo,
      this.authService.userId
    )
    // this._places.push(newPlace);
    // add(take) the first emittable place 

    // this.places.pipe(take(1)).subscribe((places)=>{
    //   setTimeout(()=>{
    //     this._places.next(places.concat(newPlace))
    //   },2000)
    // })
   return this.places.pipe(
     take(1),
     delay(2000),
     tap(places =>{
      // setTimeout(()=>{
        this._places.next(places.concat(newPlace))
      // },2000)
    }))
  }

  // update place 1
  updatePlace(placeId: string,title: string,description: string){
   return this.places.pipe(
      delay(1000),
      take(1),
      tap(places =>{
        const updatedPlaceInex = places.findIndex(pl => pl.id === placeId);
        const updatePlace = [...places];
        const oldPlace = updatePlace[updatedPlaceInex];
        updatePlace[updatedPlaceInex] = new Place(
          oldPlace.id,
          title,
          description,
          oldPlace.imgUrl,
          oldPlace.price,
          oldPlace.dateFrom,
          oldPlace.dateTo,
          oldPlace.userId
        )
        // here i emmit a new version of places 
        this._places.next(updatePlace);
      })
    )
  }


 
}
