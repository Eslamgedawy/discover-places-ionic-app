import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage {

  isLoading = false;
  isLogin = false;

  constructor(
    private authService:AuthService,
    private roter: Router,
    private loadingController: LoadingController
  ) { }


  onLogin(){
    this.isLoading = true;
    this.authService.login();

    this.loadingController.create({
      message: 'Loging In',
      keyboardClose: true

    }).then((ele)=>{

      ele.present();
      setTimeout(() => {
        this.isLoading = false;
        ele.dismiss();
        this.roter.navigateByUrl('/places/tabs/discover');
      }, 2000);

    });
  }

  onSubmit(form: NgForm){
    // console.log(form);
    if(!form.valid){
      return;
    }
    const email = form.value.email;
    const pass = form.value.password;

    console.log(email,pass);

    // if(this.isLogin){

    // }else{

    // }
  }

  onAuth(){ 
    this.isLogin = !this.isLogin;
  }
}
