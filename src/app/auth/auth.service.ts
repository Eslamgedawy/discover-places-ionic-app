import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _userIsAuth = true;
  private _userId = 'h2z';

  get userId(){
    return this._userId;
  }

  get userIsAuth(){
    console.log(this._userIsAuth);
    return this._userIsAuth;
  }

  login(){
    this._userIsAuth = true;
    console.log(this._userIsAuth);
  }

  logout(){
    this._userIsAuth = false;
    console.log(this._userIsAuth);
  }
  constructor() { }
}
