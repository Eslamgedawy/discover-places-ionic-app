import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Place } from '../../places/place.model';
import { NgForm, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-create-bookings',
  templateUrl: './create-bookings.component.html',
  styleUrls: ['./create-bookings.component.scss'],
})
export class CreateBookingsComponent implements OnInit {

  @Input() selectedPlace: Place;
  @Input() selectedMode: 'select' | 'random';
  @ViewChild('f',null) form: FormGroup; // to access any element in form
  
  sDate: string;
  eDate: string;

  constructor(private modalCtrl: ModalController) {}

  ngOnInit() {
    const dateFrom = new Date(this.selectedPlace.dateFrom);
    const dateTo = new Date(this.selectedPlace.dateTo);
    if (this.selectedMode === 'random') {
      this.sDate = new Date(
        dateFrom.getTime() +
        Math.random() * (dateTo.getTime() -7 * 24 * 60 * 60 * 1000 - dateFrom.getTime())
      ).toISOString();

      this.eDate = new Date(
        new Date(this.sDate).getTime() +
          Math.random() *
            (new Date(this.sDate).getTime() +
              6 * 24 * 60 * 60 * 1000 -
              new Date(this.sDate).getTime())
      ).toISOString();
    }
  }

  onClose() {
    this.modalCtrl.dismiss(null, 'cancel');
  }

  onBooking() {
    if (!this.form.valid || !this.datesValid) {
      return;
    }
    this.modalCtrl.dismiss(
      {
        bookingData: {
          firstName: this.form.value['first-name'],
          lastName: this.form.value['last-name'],
          guestNumber: this.form.value['guest-number'],
          startDate: this.form.value['date-from'],
          endDate: this.form.value['date-to']

        }
      },'confirm'
      );
  }

  datesValid() {
    const startDate = new Date(this.form.value['date-from']);
    const endDate = new Date(this.form.value['date-to']);
    return endDate > startDate;
  }

}
