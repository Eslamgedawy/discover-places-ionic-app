import { Injectable } from '@angular/core';
import { Booking } from './booking.model';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

 private _booking: Booking[] = [
    {
      id: 'abc',
      PlaceId: 'p1',
      placeTitle: 'Naser city',
      userId: 'abc',
      guestNumber: 1
    }
  ]

  get booking (){
    return [...this._booking];
  }
  constructor() { }
}
