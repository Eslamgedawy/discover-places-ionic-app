import { Component, OnInit } from '@angular/core';
import { BookingService } from './booking.service';
import { Booking } from './booking.model';
import { IonItemSliding } from '@ionic/angular';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit {

  loadedBookings: Booking[] ;
  constructor(private bookingServcie: BookingService) { }

  ngOnInit() {
    this.loadedBookings = this.bookingServcie.booking;
  }

  onCancel(bookingId: string,itemSliding: IonItemSliding){

    itemSliding.close();
    console.log("cancle",bookingId);
  }

}
