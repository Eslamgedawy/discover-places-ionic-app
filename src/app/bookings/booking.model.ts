export class Booking{
    constructor(
        public id: string,
        public PlaceId: string,
        public userId: string,
        public placeTitle: string,
        public guestNumber: number
    ){}
}